from rest_framework import (generics, mixins, status, views)
from rest_framework.response import Response
from .models import (Childs, Journal)
from .serializers import (ChildsSerializer, AddChildSerializer, JournalSerializer,AddJournalSerializer)
from django.shortcuts import get_object_or_404


class ChildsListView(generics.ListAPIView):
    queryset = Childs.objects.all()
    serializer_class = ChildsSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset.all(), many=True)
        return Response({'list': serializer.data})


class ChildsView(mixins.RetrieveModelMixin, generics.GenericAPIView):
    queryset = Childs.objects.all()
    lookup_url_kwarg = 'pk'
    serializer_class = ChildsSerializer

    def get(self, request, *args, **kwargs):
        response = self.retrieve(request, *args, **kwargs)
        return response


class ChildsCreate(generics.CreateAPIView):
    serializer_class = AddChildSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ChildsUpdate(views.APIView):
    queryset = Childs.objects.all()
    serializer_class = AddChildSerializer

    def post(self, request, *args, **kwargs):
        data = self.request.data
        name = data.get('name', None)
        bday = data.get('bday', None)
        class_num = data.get('class_num', None)
        studying = data.get('studying', None)
        data_upd = {}
        if name:
            data_upd['name'] = name
        if bday:
            data_upd['bday'] = bday
        if class_num:
            data_upd['class_num'] = class_num
        if studying:
            data_upd['studying'] = studying
        res = self.queryset.filter(pk=self.kwargs['pk']).update(**data_upd)

        return Response(res, status=status.HTTP_200_OK)


class JournalListView(generics.ListAPIView):
    queryset = Journal.objects.filter(child__studying=True)
    serializer_class = JournalSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(self.queryset.all(), many=True)
        return Response({'list': serializer.data})


class JournalCreate(generics.CreateAPIView):
    serializer_class = AddJournalSerializer

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)
