from django.apps import AppConfig


class ChildsConfig(AppConfig):
    name = 'nursery.apps.childs'
