from rest_framework import serializers
from .models import (Childs, Journal)


class JournalSerializer(serializers.ModelSerializer):
    date = serializers.DateField()
    time = serializers.TimeField()

    class Meta:
        model = Journal
        fields = ('id', 'date', 'time', 'parent', 'came_gone')


class ChildsSerializer(serializers.ModelSerializer):
    journal = JournalSerializer(many=True, required=False)
    photo = serializers.ImageField(use_url=False)
    bday = serializers.DateField()

    class Meta:
        model = Childs
        fields = ('id', 'photo', 'name', 'bday', 'class_num', 'studying', 'journal')


class AddChildSerializer(serializers.ModelSerializer):
    bday = serializers.DateField()
    class Meta:
        model = Childs
        fields = ( 'name', 'bday', 'class_num', 'studying')


class AddJournalSerializer(serializers.ModelSerializer):
    date = serializers.DateField()
    time = serializers.TimeField()
    child = serializers.PrimaryKeyRelatedField(
        queryset=Childs.objects.all())
    class Meta:
        model = Journal
        fields = ('date', 'time', 'parent', 'came_gone','child')
