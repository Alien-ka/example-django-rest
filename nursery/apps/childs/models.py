from django.db import models
from django.utils import timezone

class Childs(models.Model):
    CLASS_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
    )
    photo = models.ImageField(verbose_name='фото', upload_to='childs/', null=True, blank=True)
    name = models.CharField(verbose_name='имя', max_length=1047)
    bday = models.DateField(verbose_name='дата рождения')
    class_num = models.SmallIntegerField(choices=CLASS_CHOICES, verbose_name='класс', default=1)
    studying = models.BooleanField(default=True, verbose_name='учится')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'ребенок'
        verbose_name_plural = 'дети'


class Journal(models.Model):
    PARENT_CHOICES = (
        (1, 'мать'),
        (2, 'отец')
    )
    CAME_GONE_CHOICES = (
        (1, 'привели'),
        (2, 'забрали')
    )
    child = models.ForeignKey(Childs, verbose_name='ребенок', related_name='journal',default=None,on_delete=models.CASCADE)
    date = models.DateField(verbose_name='дата', default=timezone.now,editable=True)
    time = models.TimeField(verbose_name='время', default=timezone.now,editable=True)

    came_gone = models.SmallIntegerField(choices=CAME_GONE_CHOICES, verbose_name='привели/забрали', default=1)
    parent = models.SmallIntegerField(choices=PARENT_CHOICES, verbose_name='родитель', default=1)

    def __str__(self):
        return self.child.name if self.child else self.date

    class Meta:
        ordering = ('-date','time')
        verbose_name = 'журнал'
        verbose_name_plural = 'журнал'
