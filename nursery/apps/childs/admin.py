from django.contrib import admin
from django.contrib.admin import ModelAdmin
from .models import Childs, Journal


class ChildsAdmin(ModelAdmin):
    list_display = ('name', 'bday', 'class_num', 'studying')
    list_filter = ['class_num', 'studying', ]
    search_fields = ('name',)


class JournalAdmin(ModelAdmin):
    list_display = ('__str__', 'time', 'parent', 'date', 'came_gone')
    list_filter = ['date', ]


admin.site.register(Childs, ChildsAdmin)
admin.site.register(Journal, JournalAdmin)
