from rest_framework import status
from rest_framework.test import APIClient
from django.test import TestCase
from .models import Childs, Journal


class ChildsTestCase(TestCase):
    def setUp(self):
        self.child = Childs.objects.create(studying=False, bday='2015-06-15', name='old name0', class_num=1)
        self.client = APIClient()

    def test_childs_add(self):
        request = self.client.post('/api/childs/add/', {
            'studying': True,
            'bday': '2018-06-15',
            'name': 'new name1',
            'class_num': 1
        })
        childs = Childs.objects.all()

        self.assertEqual(len(childs), 2)
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)

    def test_childs_upd(self):
        request = self.client.post('/api/childs/upd/'+str(self.child.id)+'/', {
            'studying': False,
            'bday': '2000-06-15',
            'name': 'new name3',
            'class_num': 3
        })

        child = Childs.objects.get(id=self.child.id)
        self.assertEqual(child.name, 'new name3')
        self.assertEqual(request.status_code, status.HTTP_200_OK)

    def test_journal(self):
        request = self.client.post('/api/childs/journal/add/', {
            "date": "2018-06-13",
            "time": "08:05:00",
            "parent": 1,
            "came_gone": 1,
            "child": self.child.id
        })
        journal = Journal.objects.all()
        self.assertEqual(request.status_code, status.HTTP_201_CREATED)
        self.assertEqual(len(journal), 1)
