from django.conf.urls import url
from .views import (ChildsListView, ChildsView, ChildsCreate, ChildsUpdate, JournalCreate, JournalListView)

urlpatterns = [
    url('^(?P<pk>\d+)/', ChildsView.as_view(), name='childs-detail'),
    url('^upd/(?P<pk>\d+)/', ChildsUpdate.as_view(), name='childs-upd'),
    url('^add/', ChildsCreate.as_view(), name='childs-add'),
    url('^$', ChildsListView.as_view(), name='childs-list'),

    url('^journal/add/', JournalCreate.as_view(), name='journal-add'),
    url('^journal/', JournalListView.as_view(), name='journal-list'),
]
